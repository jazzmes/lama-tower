/**
 * Created by tiago on 2/18/16.
 */

angular.module('lamaTowerApp')
    .service('lamaConfig', function (store) {
        this.dispatcher = store.get('dispatcher') || {
                ip: "10.1.1.12",
                port: 20001,
                exp_server_port: 21000
            };
        this.appTypes = store.get('appTypes') || {
                'cmart-lb-tier-sql': {defaultParams: 'peak_users=300,run_length=5,rampup_time=10'},
                'cmart-lb-tier-sql-bare_images': {defaultParams: 'peak_users=300,run_length=5,rampup_time=10'},
                'rubbos-nolb': {defaultParams: ''},
                'cirros-single': {defaultParams: ''}
            };
        this.defaults = {
            appType: 'cirros-single',
            appName: 'cirros'
        };
        console.log("Dispatcher initialized", this.dispatcher);

        this.setOptions = function (key, values) {
            store.set(key, values);
        };

        return this;
    });

