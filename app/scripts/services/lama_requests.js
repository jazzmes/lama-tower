/**
 * Created by tiago on 2/18/16.
 */

// author: Tiago Carvalho

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}


function LamaRequests(dispatchers) {
    //console.log("Given dispatchers:", dispatchers);
    this.dispatchers = dispatchers || [];
    //console.log("Current dispatchers:", this.dispatchers);
}

LamaRequests.prototype.add_dispatcher = function (dispatcher) {
    var ip = dispatcher.ip_address || dispatcher.ip;
    if (!ip) {
        console.error("Bad dispatcher - no IP", dispatcher);
        return false;
    } else if (!dispatcher.port) {
        console.error("Bad dispatcher - no Port", dispatcher);
        return false;
    }
    dispatcher.ip_address = ip;
    for (var i = 0; i < this.dispatchers.length; i++) {
        if (dispatcher.ip_address == this.dispatchers[i].ip_address) {
            this.dispatchers[i].port = dispatcher.port;
        }
    }
    this.dispatchers.push(dispatcher);
    return true;
};

LamaRequests.prototype.ajax = function (url, options) {
    var req_options = {
        url: url,
        headers: {"X-CSRFToken": getCookie("csrftoken")},
        type: 'POST',
        cache: false,
        dataType: "json",
        contentType: "application/json",
        timeout: 3000
    };
    var o = $.extend({}, req_options, options, {
        success: function (data, textStatus, jqXHR) {
            //console.log("success!", data, textStatus, jqXHR);
            if (data && data.hasOwnProperty("result")) {
                if (data.result) {
                    //console.log("Success", data, textStatus, jqXHR);
                    return options.success(data, textStatus, jqXHR);
                } else {
                    console.error("LAMA Error: ", data.hasOwnProperty("msg") ? data.msg : data);
                }
            } else {
                console.error("BAD data: ", data, textStatus, jqXHR);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            if (options.hasOwnProperty('error')) {
                return options.error(xhr, ajaxOptions, thrownError);
            } else {
                console.log("error!", xhr, ajaxOptions, thrownError);
            }
        }
    });

    if (o && o.hasOwnProperty("data")) {
        o.data = JSON.stringify(o.data);
    }
    //console.log("Request: ", o);
    $.ajax(o);
};

LamaRequests.prototype.dispatcher = function (action, options) {
    console.debug("request from dispatcher: ", this.dispatchers);
    if (this.dispatchers.length == 0) {
        console.debug("no dispatchers available");
        this.web("dispatchers", {
            success: function (data) {
                console.debug("success!");
                if (data.hasOwnProperty("dispatchers")) {
                    if (data.dispatchers.length > 0) {
                        this.dispatchers = data.dispatchers;
                    }
                }
                this.dispatcher(action, options);
            }.bind(this)
        });
    } else {
        console.debug("loading data");
        this.agent(this.dispatchers[0].ip_address, this.dispatchers[0].port, action, options);
    }
};

LamaRequests.prototype.provider = function (host, port, action, options) {
    this.agent(host, port, action, options);
};

LamaRequests.prototype.web = function (action, options) {
    var url = "/monitor/" + action + "/";
    this.ajax(url, options);
};

LamaRequests.prototype.agent = function (host, port, action, options) {
    var url = "http://" + host + ":" + port + "/";
    options = options || {};
    if (options.hasOwnProperty("data")) {
        options.data["action"] = action;
    } else {
        options.data = {
            action: action
        };
    }

//    console.log("Requesting from", url, " with ", options);
    this.ajax(url, options);
};

LamaRequests.prototype.file_upload = function (host, port, action, options) {


    var url = "http://" + host + ":" + port + "/";
//    console.log("I am here!");
    if (options && options.hasOwnProperty("data")) {
        options.data.append("action", action);
        console.log("appended!");
    } else {
        console.log("no data...");
    }

    var final_options = $.extend({}, {
        url: url,
        type: 'POST',
//        xhr: function () {  // Custom XMLHttpRequest
//            var myXhr = $.ajaxSettings.xhr();
//            if (myXhr.upload) { // Check if upload property exists
//                myXhr.upload.addEventListener('progress', on_progress, false); // For handling the progress of the upload
//            }
//            return myXhr;
//        },
//        success: function(data){
//            console.log(data);
//        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("error", xhr, ajaxOptions, thrownError);
        },
        cache: false,
        contentType: false,
        processData: false
    }, options);

    console.log(final_options);
    $.ajax(final_options);

};

angular.module('lamaTowerApp')
    .service('lamaRequests', function (lamaConfig) {
        console.log("new lama requests object");
        this._requests = new LamaRequests();
        this._requests.add_dispatcher(lamaConfig.dispatcher);
        console.log("requests: ", this._requests);
        return this._requests;
    });

