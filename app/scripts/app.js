'use strict';

/**
 * @ngdoc overview
 * @name lamaTowerApp
 * @description
 * # lamaTowerApp
 *
 * Main module of the application.
 */
angular
  .module('lamaTowerApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ngStorage',

    'ui.router',
    'ui.bootstrap',

    'xenon.controllers',
    'xenon.directives',
    'xenon.factory',
    'xenon.services',

    'FBAngular',
    'oc.lazyLoad',
    'datatables',

    'angular-storage'

  ])
  .config(function ($stateProvider, $urlRouterProvider) { //}, $ocLazyLoadProvider, ASSETS) {
    $urlRouterProvider.otherwise('/lama/dashboard');

    $stateProvider
      .state('lama', {
        abstract: true,
        url: '/lama',
        templateUrl: 'views/xenon/layout/app-body.html',
        controller: function ($rootScope) {
          $rootScope.isLoginPage = false;
          $rootScope.isLightLoginPage = false;
          $rootScope.isLockscreenPage = false;
          $rootScope.isMainPage = true;
        }
      })

      // Main Layout Structure
      .state('lama.dashboard', {
        url: '/dashboard',
        templateUrl: 'views/dashboard.html',
        controller: 'LamaDashboardCtrl'
      })
      .state('lama.providers', {
        url: '/providers',
        templateUrl: 'views/providers.html',
        controller: 'LamaProvidersCtrl',
        resolve: {
          resources: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              'scripts/directives/peer_graph.js',
              'scripts/directives/dyn_table.js',
              'styles/providers.css',
              //'scripts/lib/datatables-angular.js',
              //'styles/lib/dataTables.bootstrap.css'
            ]);
          }
        }
      })
      .state('lama.apps', {
        url: '/apps/:appname',
        templateUrl: 'views/apps.html',
        controller: 'LamaAppsCtrl',
        resolve: {
          resources: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              'scripts/directives/app_status_graph.js',
              'styles/app_status_graph.css',
              'styles/apps.css',
              'scripts/lib/dagre-d3.js',
              'scripts/directives/epoch_charts.js'
            ]);
          }
        }
      })
      .state('lama.experiments', {
        url: '/experiments',
        templateUrl: 'views/experiments.html',
        controller: 'LamaExperimentsCtrl',
        resolve: {
          resources: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              'styles/experiments.css',
              'scripts/lib/datatables-angular.js',
              'styles/lib/dataTables.bootstrap.css',


              //      ASSETS.forms.inputmask,
              //      ASSETS.tables.datatables,
              //      ASSETS.lama.css.experiments,
              //      ASSETS.extra.toastr
            ]);
          }
        }
      })
      .state('lama.manage', {
        url: '/manage',
        templateUrl: 'views/manage.html',
        controller: 'LamaManageCtrl'
      })
    ;
  });

