'use strict';

/**
 * @ngdoc function
 * @name lamaTowerApp.controller:LamaAppsCtrl
 * @description
 * # LamaAppsCtrl
 * Controller of the LAMA Apps section: App visualization
 */
var RealTimeTestData = function (layers) {
    this.layers = layers;
    this.timestamp = ((new Date()).getTime() / 1000) | 0;
};

RealTimeTestData.prototype.rand = function () {
    return parseInt(Math.random() * 100) + 50;
};

RealTimeTestData.prototype.history = function (entries) {
    if (typeof(entries) != 'number' || !entries) {
        entries = 60;
    }

    var history = [];
    for (var k = 0; k < this.layers; k++) {
        history.push({values: []});
    }

    for (var i = 0; i < entries; i++) {
        for (var j = 0; j < this.layers; j++) {
            history[j].values.push({time: this.timestamp, y: this.rand()});
        }
        this.timestamp++;
    }

    return history;
};

RealTimeTestData.prototype.next = function () {
    var entry = [];
    for (var i = 0; i < this.layers; i++) {
        entry.push({time: this.timestamp, y: this.rand()});
    }
    this.timestamp++;
    return entry;
};

//var liveAreaData = new RealTimeTestData(1);

angular.module('lamaTowerApp')
    .controller('LamaAppsCtrl',
        function ($scope, $rootScope, $stateParams, lamaRequests, $timeout) {
            //$scope.absUrl = $location.absUrl();
            $scope.toggle_pause = function ($event) {
                var val = $event.toElement.checked;
                if (val) {
                    $scope.dashboard.restart();
                } else {
                    $scope.dashboard.pause();
                }
            };
            $scope.toggle_filter_sup = function ($event) {
                var val = $event.toElement.checked;
                if (val) {
                    $scope.dashboard.restart();
                } else {
                    $scope.dashboard.pause();
                }
            };

            $scope.loadApps = function () {
                lamaRequests.dispatcher('app_list', {
                    //$http.post('http://' + lamaConfig.dispatcher.ip + ':' + lamaConfig.dispatcher.port,
                    //  {
                    //    action: 'app_list'
                    //  }
                    //  )
                    success: function (data, status, headers, config) {
                        //console.log("Load apps response", data);
                        if (data.result) {
                            $scope.availableApps = $.map(data.apps, function (e) {
                                var opt = {'label': e.name, 'value': e.name, 'ip': e.provider, 'port': e.provider_port};
                                if ($stateParams.appname == e.name)
                                    console.log("changing current app");
                                $scope.currentApp = opt;
                                return opt;
                            });
                        }
                        //console.log($scope.availableApps);
                    },
                    error: function (data, status, headers, config) {
                        console.error(data);
                    }
                });
            };

            $scope.getObjVal = function (obj, keys) {
                //console.log("Get", obj, keys);
                if (!obj)
                    return null;
                var first_key = keys.shift();
                var new_obj = obj[first_key];
                //console.log("new_obj_keys", new_obj, keys);
                return keys.length > 0 ? $scope.getObjVal(new_obj, keys) : new_obj;
            };

            $scope.getMonitorData = function () {
                console.log("MonitorData", $scope.currentApp);
                if ($scope.currentApp) {
                    lamaRequests.provider($scope.currentApp.ip, $scope.currentApp.port, 'app_monitor_data',
                        {
                            data: {
                                app_name: $scope.currentApp.value,
                                options: {
                                    filters: {
                                        hosts: ['10.1.1.31'],
                                        //services: ['client', 'lb-mysqlc-sql', 'lb-apache', ],
                                        metrics: ['response_time', 'cpu']
                                    }
                                }
                            },
                            success: function (data, status, header, config) {
                                console.log("MonitorData RESP", data);
                                $scope.agentData = data;

                                //var timestamp = ((new Date()).getTime() / 1000)|0;
                                var ks = ['hosts', "10.1.1.31", 'cpu', 0, 'cpu-idle', 0];
                                var cpu_idle_value = $scope.getObjVal(data, ks);
                                if (cpu_idle_value) {
                                    var new_entry = [
                                        {
                                            time: Number((cpu_idle_value[0] / 1000).toFixed(0)),
                                            y: Number((100 - cpu_idle_value[1]).toFixed(1))
                                        }
                                    ];
                                    $scope.response_time_chart.prevValue = $scope.response_time_chart.value;
                                    $scope.response_time_chart.value = new_entry[0].y;
                                    $scope.response_time_chart.feed = new_entry;
                                }

                                $timeout($scope.getMonitorData, 10000);
                            },
                            error: function (data, status, headers, config) {
                                console.error("MonitorData ERR", data);
                                $timeout($scope.getMonitorData, 10000);
                            }
                        }
                    );
                } else {
                    console.warn("MonitorData", "No app currently selected!");
                }

            };

//      $scope.getMetrics = function() {
//        if ($scope.currentApp) {
//          lamaRequests.provider($scope.currentApp.ip + ":" + $scope.currentApp.port,
//            {
//              action: "app_agent_monitor_data",
//              app_name: $scope.currentApp.value
//            }
//          ).success(function(data, status, header, config){
//            console.log(data);
//            $scope.agentData = data;
//          });
//        } else {
//          console.warn("No app currently selected!");
//        }
//
//
//        lamaRequests.provider();
//        $.get("http://" + this.server + ":" + this.port + "/metrics", params,
//          function(data, status, jqxhr){
////                console.log("Success:Data: ", jqxhr.responseText);
//            console.log("Success:Data Size", jqxhr.responseText.length);
//            alert_p.removeClass();
//            alert_p.addClass("success");
//            alert_p.text("Connection active - Last update: " + new Date());
//            this.minMaxTs = -1;
//            this.refreshPage(data);
//          }.bind(this),
//          "json"
//        ).fail(function(){
//          this.handleFail("server unreachable! Last attempt: " + new Date());
//        }.bind(this));
//      };

            // main
            $scope.agentData = {};
            $scope.metrics = {};
            $scope.response_time_chart = {
                title: 'CPU ?', //10.1.1.31',
                options: {
                    axes: ['left', 'right', 'bottom']
                },
                data: [{
                    values: [{time: ((new Date()).getTime() / 1000) | 0, y: 0}]
                }],
                feed: [{}],
                value: 0,
                prevValue: 0
            };

            $scope.availableApps = null;
            $scope.currentApp = null;

            $scope.loadApps();

            $scope.$watch('currentApp', $scope.getMonitorData);
//            console.log("watching current app");
            //        $scope.$watch("currentApp", $scope.getCurrentAppInfo);
            //        $scope.$watch("currentApp", $scope.loadEvents);

        });
