'use strict';

/**
 * @ngdoc function
 * @name lamaTowerApp.controller:LamaManageCtrl
 * @description
 * # LamaManageCtrl
 * Controller of the LAMA Manage section
 */
angular.module('lamaTowerApp')
  .controller('LamaManageCtrl',
    function ($scope, $rootScope, lamaConfig, $uibModal) {
      $scope.openModal = function(modal_id, action)
      {
        var currentModal = $uibModal.open({
          animations: true,
          controller: 'ModalInstanceCtrl',
          templateUrl: modal_id,
          size: 'sm',
          backdrop: true
        });

        currentModal.result.then($scope.executeAction(action));
      };

      $scope.executeAction = function(action) {
        console.log("Executing", action);
      };

      console.log(lamaConfig, this);
      $scope.dispatcher = lamaConfig.dispatcher;
      $scope.saveDispatcher = function(){
        lamaConfig.setOptions('dispatcher', lamaConfig.dispatcher);
      };
    })
  .controller('ModalInstanceCtrl', function ($scope, $uibModalInstance, action) {

    $scope.action = action;
    console.log(action, $scope.action);

    $scope.ok = function () {
      $uibModalInstance.close($scope.action);
    };

    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  });;
