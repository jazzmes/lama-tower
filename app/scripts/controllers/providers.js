'use strict';

/**
 * @ngdoc function
 * @name lamaTowerApp.controller:LamaProvidersCtrl
 * @description
 * # LamaProvidersCtrl
 * Controller of the LAMA Providers section: PRovider visualization
 */
angular.module('lamaTowerApp')
  .controller('LamaProvidersCtrl', function ($scope, $rootScope, $ocLazyLoad, lamaRequests, DTOptionsBuilder, DTColumnBuilder) {
    // functions
    $scope.parseMetricValue = function (valstr) {
      var re = /([0-9]*\.[0-9]*|[0-9]+)([kMG]?)/g;
      var res = re.exec(valstr);
      var val = 0;
      if (res && res[1])
        val = parseFloat(res[1]);

      if (res[2] && modifiers[res[2]])
        val *= modifiers[res[2]];
      return val;
    };

    $scope.loadProviderInfo = function (provider) {
      if ($scope.currentProviderName === provider.name) {
        $scope.$apply(function () {
          $scope.providerInfo = null;
          $scope.currentProviderName = null;
        });
        return;
      }

      if (provider.hasOwnProperty("name") && provider.hasOwnProperty("port")) {
        console.log("request from provider");
        lamaRequests.provider(provider.name, provider.port, "agent_info", {
            success: function (data) {
              // pre-process resources for data table
              if (!data.result) {
                console.error("Error loading info:", data.msg);
                $scope.providerInfo = null;
                $scope.currentProviderName = null;
                $scope.resourcesTable.data = null;
              } else {
                var allocations = {};
                console.log(data);
                $.each(data.allocations, function (i, alloc) {
                  if (alloc.hasOwnProperty("spec")) {
                    $.each(alloc.spec, function (rname, allocs) {
//                                    console.log(i, allocs);
                      $.each(allocs, function (j, metric) {
                        $.each(metric, function (mname, valstr) {
                          if (valstr) {
                            var val = $scope.parseMetricValue(valstr);
//                                                console.log(mname, val);
                            if (!allocations[rname]) {
                              allocations[rname] = {};
                            }
                            if (!allocations[rname][mname]) {
                              allocations[rname][mname] = val;
                            } else {
                              allocations[rname][mname] += val;
                            }
//                                                console.log(allocations);
                          }
                        });
                      });
                    });
                  }
                });

                var resources = [];
                $.each(data.resources, function (rname, res) {
                  $.each(res, function (index, entry) {
                    $.each(entry, function (mname, resource) {
                      if (resource) {
                        var val = $scope.parseMetricValue(resource);
                        var alloc = "";
                        if (allocations[rname] && allocations[rname][mname]) {
                          alloc = allocations[rname][mname];
                        }
//                                        console.log(val, alloc, alloc/val);
                        resources.push(
                          {
                            comp: rname,
                            metric: mname,
                            key: rname + "-" + mname + "-" + index,
                            value: resource,
                            allocated: "" + (alloc * 100 / val).toFixed(2) + "%"
                          }
                        );
                      }
                    });
                  });
                });
                //                        data.data = resources;

                angular.copy(resources, $scope.resources);
                $scope.providerInfo = data;
                $scope.currentProviderName = provider.name;
                $scope.$digest();
              }
            },
            error: function (x, y, z) {
              console.error("Error loading info:", x, y, z);
              $scope.providerInfo = null;
              $scope.currentProviderName = null;
              $scope.$digest();
            }
          }
        );
      }

    };

    // local variables
    var modifiers = {
      "k": 1E3,
      "M": 1E6,
      "G": 1E9
    };

    // bind to peer graph event
    $scope.provider_clicked = $scope.loadProviderInfo;
    $scope.providerInfo = null;
    $scope.currentProviderName = null;

    $scope.resources = [];
  })
  .controller('ResourcesDtCtrl',
    function ($scope, DTOptionsBuilder, DTColumnDefBuilder) {
      // resources table
      var dt = this;
      dt.resources = $scope.$parent.resources;
      dt.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('paging', false)
        .withOption('searching', false)
      ;
      //.withDOM('pitrfl');
      dt.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2).notSortable(),
        DTColumnDefBuilder.newColumnDef(3).notSortable(),
        DTColumnDefBuilder.newColumnDef(4)
      ];
    });
