//'use strict';

/**
 * @ngdoc function
 * @name lamaTowerApp.controller:LamaExperimentsCtrl
 * @description
 * # LamaExperimentsCtrl
 * Controller of the LAMA Experiments section
 */

var UniqueId = function (start) {
    this.index = start || 1;
};

UniqueId.prototype.getIndex = function () {
    return this.index++;
};

angular.module('lamaTowerApp')
    .controller('LamaExperimentsCtrl',
        function ($scope, $rootScope, lamaConfig, lamaRequests, $timeout, $http) {
            //// to handle tabs... but is not working (the event fires but adjusting does not work
            //$scope.onTabSelect = function(){
            //    console.log("tab selected", $.fn.dataTable.tables(), $.fn.dataTable.tables( {visible: true, api: true} ));
            //    $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
            //};
            $scope.loadApps = function () {
                console.log("@loadApps");
                lamaRequests.dispatcher('app_list', {
                    success: function (data, status, headers, config) {
                        if (data.result) {
                            console.log("Apps", data.apps);
                            $.each(data.apps, function (i, app) {
                                $scope.appEntries[app.name] = app;
                            });
                        }
                    },
                    error: function (x, y, z) {
                        console.error("Error loading info:", x, y, z);
                        $scope.providerInfo = null;
                        $scope.currentProviderName = null;
                        $scope.$digest();
                    },
                    timeout: 3000
                });
            };

            //$scope.updateAppStatus = function (id, status) {
            //  $scope.setEntryAttr($scope.appTable, $scope.appEntries[id], "Status", status);
            //};

            $scope.adjustName = function (oldName, newName) {
                $scope.appEntries[newName] = $scope.appEntries[oldName];
                $scope.appEntries[newName].name = newName;
                delete $scope.appEntries[oldName];

                // update app request with last name
                if ($scope.appRequest.appName == oldName)
                    $scope.appRequest.appName = newName;
            };

            $scope.updateStatus = function (tmpName, data, totalWait) {
                totalWait = totalWait || 0;
                console.log("update status", tmpName, data);
                //                    console.log($scope.appRequest.appName);
                // find app
                var key = tmpName;
                if (data.hasOwnProperty("appname")) {
                    //                        $scope.appEntries[tmpId]] = data.appname;
                    if (data.appname != tmpName)
                        $scope.adjustName(tmpName, data.appname);
                    //$scope.setEntryAttr($scope.appTable, $scope.appEntries[tmpId], "App Name", data.appname);
//                            $scope.getEntryAttr($scope.appTable, $scope.appEntries[tmpId], "App Name");
                    console.log(data.appname, $scope.appEntries);
                    key = data.appname;
                }

                var app = $scope.appEntries[key];
                app.msg = data.msg;
                var done = true;
                if (data.complete) {
                    console.log("complete");
                    if (data.result) {
                        toastr.success("Application '" + app.name + "' was deployed at " + app.provider, "Application Deployed", {
                            "closeButton": true, "debug": false, timeout: 0, extendedTimeout: 2
                        });
                        app.status = 'Deployed';
                    } else {
                        console.log("not result");
                        app.status = 'Failed';
                    }
                } else {
                    console.log("not complete");
                    if (data.result) {
                        app.status = 'Deploying...';
                    } else {
                        app.status = 'Failed?';
                    }
                    console.log("result not complete", app.status);

                    // check again later
                    //timeoutVal = timeoutVal || 5000;
                    var timeoutVal = 10000;
                    totalWait += timeoutVal;
                    console.log("timeout?", timeoutVal);
                    if (totalWait < 1200000) {
                        console.log("Rechecking in " + timeoutVal);
                        $timeout($scope.checkStatus, timeoutVal, true, key, totalWait);
                    } else {
                        console.warn("Gave up checking after 20min");
                        toastr.warning("Gave up checking application '" + app.name + "' (@" + app.provider +") after 20min.", "Stopped Checking Status", {
                            "closeButton": true, "debug": false
                        });

                        app.status = "Unknown";
                        app.msg = 'Gave up checking after 20min.';
                        //$timeout($scope.checkStatus, timeoutVal, true, key, timeoutVal);
                    }
                }
                return done;
            };

            $scope.deployApp = function (appRequest) {
                if (!$scope.dispatcher.ip || !$scope.dispatcher.exp_server_port)
                    return;

                var tmpName = $scope.appRequest.appName;
                while ($scope.appEntries.hasOwnProperty(tmpName)) {
                    tmpName += '-' + $scope.uniqueId.getIndex();
                }

                $scope.appEntries[tmpName] = {
                    name: $scope.appRequest.appName,
                    app_type: $scope.appRequest.appType,
                    status: "Deploying..."
                };
                $http.post('http://' + $scope.dispatcher.ip + ':' + $scope.dispatcher.exp_server_port,
                    {
                        action: 'start-app',
                        appname: $scope.appRequest.appName,
                        apptype: $scope.appRequest.appType
                    }
                ).success(
                    function (data, status, headers, config) {
                        $scope.updateStatus(tmpName, data);
                    }
                ).error(
                    function (data, status, headers, config) {
                        toastr.error("Unable to contact experiment server '" + $scope.dispatcher.ip + ':' + $scope.dispatcher.exp_server_port + "'.", "Error deploying app", {
                            "closeButton": true, "debug": false
                        });
                        app.status = 'Failed';
                        //$scope.updateAppStatus(tmpId, "<p class='status status-fail' title='Promise error'>Failed</p>");
                    }
                );

            };
            $scope.uniqueId = new UniqueId();

            $scope.checkStatus = function (key, timeoutVal) {
                console.log("Checking status: ", key);
                var app = $scope.appEntries[key] || {};
                if (!app)
                    console.warn("Unable to check status for app", key);

                $http.post('http://' + $scope.dispatcher.ip + ':' + $scope.dispatcher.exp_server_port,
                    {
                        action: 'check-status',
                        appname: app.name
                    }
                ).success(
                    function (data, status, headers, config) {
                        $scope.updateStatus(key, data, timeoutVal);
                    }
                ).error(
                    function (data, status, headers, config) {
                        app.status = 'Failed';
                    }
                );
            };

            // main
            $scope.dispatcher = lamaConfig.dispatcher;

            $scope.appTypes = Object.keys(lamaConfig.appTypes);
            // app entries indexed by id
            $scope.appEntries = {};
            $scope.appRequest = {
                appName: lamaConfig.defaults.appName,
                appType: lamaConfig.defaults.appType
            };
            $scope.loadApps();
        })
    .controller('AppsDtCtrl',
        function ($scope, DTOptionsBuilder, DTColumnDefBuilder) {
            var dt = this;
            this.detail = function (idx) {
                console.log("View detail of", idx, dt.apps[idx].app_name);
            };

            this.delete = function (idx) {
                console.log("Delete", idx, dt.apps[idx].app_name);
            };

            this.getStatusClass = function (status) {
                switch (status) {
                    case 'Deployed':
                        return 'status-success';
                    case 'Failed':
                        return 'status-fail';
                    default:
                        return 'status-unknown';
                }
            };
            // resources table
            dt.apps = $scope.appEntries;
            dt.dtOptions = DTOptionsBuilder.newOptions()
                .withOption('paging', false)
                .withOption('searching', false)
            ;
            //.withDOM('pitrfl');
            dt.dtColumnDefs = [
                DTColumnDefBuilder.newColumnDef(0),
                DTColumnDefBuilder.newColumnDef(1),
                DTColumnDefBuilder.newColumnDef(2).notVisible(),
                DTColumnDefBuilder.newColumnDef(3),
                DTColumnDefBuilder.newColumnDef(4).notSortable().notVisible(),
                DTColumnDefBuilder.newColumnDef(5).notSortable()
            ];
        })
    .controller('ExpClientsCtrl',
        function ($scope, $rootScope, $http, $interval, lamaConfig) {
            console.log("ClientsCtrl initialized");

            $scope.processParameters = function (params) {
                var res = {};
                if (params) {
                    params = params.split(",");
                    $.each(params, function (i, ps) {
                        console.log(ps);
                        var v = ps.split("=");
                        res[v[0]] = v[1];
                    })
                }
                return res;
            };

            $scope.addSlashes = function (str) {
                return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
            };

            $scope.launchClient = function () {
                if (!lamaConfig.dispatcher.ip || !lamaConfig.dispatcher.exp_server_port) {
                    toastr.error("Experiment server (ip, port) is not configured.", "Error launching client", {
                        "closeButton": true, "debug": false
                    });
                    return;
                }

                var appName = $scope.currentClientApp.name;
                if (!appName) {
                    toastr.error("Unable to retrieve app name.", "Error launching client", {
                        "closeButton": true, "debug": false
                    });
                    return;
                }

                var params = $scope.processParameters($scope.currentClientParams);
                var clientId = $scope.clientId.getIndex();
                $scope.clientEntries[clientId] = {
                    app_name: $scope.currentClientApp.name,
                    name: "<i>Not available yet...</i>",
                    params: $.map(params, function (v, k) {
                        return k + " = " + v;
                    }).join('<br/>'),
                    status: "Launching...",
                    client_id: clientId
                };

                $http.post('http://' + lamaConfig.dispatcher.ip + ':' + lamaConfig.dispatcher.exp_server_port,
                    {
                        action: 'launch-client',
                        appname: appName,
                        config: params
                    }
                ).success(
                    function (data, status, headers, config) {
                        console.log(data, status, headers, config);
                        if (data.result) {
                            toastr.success("Need to continue processing...", "Response for client launched request received!");
                            $scope.clientEntries[clientId] = "Accepted";
                        } else {
                            console.warn(data);
                            toastr.error("Error sent by server: " + $scope.addSlashes(data.msg), "Error for client launched request!");
                            $scope.clientEntries[clientId] = "Failed";
                        }
                    }
                ).error(
                    function (data, status, headers, config) {
//                        $scope.updateAppStatus(tmpId, "<p class='status status-fail' title='Promise error'>Failed</p>");
                        toastr.error("Unable to send request to server: " + data, "Error on Launch Client Request");
                    }
                );

            };

            // main
            $scope.currentClientApp = $scope.appEntries[$scope.appRequest.appName];
            $scope.currentClientParams = '';

            $scope.clientEntries = {};
            $scope.clientId = new UniqueId();

            $scope.$watch('appEntries', function() {
                var app = $scope.appEntries[$scope.appRequest.appName] || $scope.appEntries[Object.keys($scope.appEntries)[0]];
                if (app) {
                    $scope.currentClientApp = app;
                }
            }, true);
            $scope.$watch('currentClientApp', function() {
                var app = $scope.currentClientApp;
                if (app) {
                    $scope.currentClientParams = (lamaConfig.appTypes[app.app_type] || {}).defaultParams;
                }
            });

        }
    )
    .controller('ExpAppsCtrl',
        function ($scope, $rootScope, $http, $interval, lamaConfig) {
        }
    )
    .controller('ClientsDtCtrl',
        function ($scope, DTOptionsBuilder, DTColumnDefBuilder) {
            var dt = this;
            this.detail = function (idx) {
                console.log("View detail of", idx, dt.apps[idx].app_name);
            };

            this.delete = function (idx) {
                console.log("Delete", idx, dt.apps[idx].app_name);
            };

            this.getStatusClass = function (status) {
                switch (status) {
                    case 'Deployed':
                        return 'status-success';
                    case 'Accepted':
                        return 'status-success';
                    case 'Failed':
                        return 'status-fail';
                    default:
                        return 'status-unknown';
                }
            };

            // resources table
            dt.clients = $scope.clientEntries;
            dt.dtOptions = DTOptionsBuilder.newOptions()
                .withOption('paging', false)
                .withOption('searching', false)
            ;
            //.withDOM('pitrfl');
            dt.dtColumnDefs = [
                DTColumnDefBuilder.newColumnDef(0),
                DTColumnDefBuilder.newColumnDef(1),
                DTColumnDefBuilder.newColumnDef(2),
                DTColumnDefBuilder.newColumnDef(3).notSortable(),
                DTColumnDefBuilder.newColumnDef(4),
                DTColumnDefBuilder.newColumnDef(5),
                DTColumnDefBuilder.newColumnDef(6),
                DTColumnDefBuilder.newColumnDef(7).notSortable(),
            ];
        });
