'use strict';

/**
 * @ngdoc function
 * @name lamaTowerApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the lamaTowerApp
 */
angular.module('lamaTowerApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
