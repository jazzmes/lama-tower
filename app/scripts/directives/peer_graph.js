/**
 * Created by tiago on 3/6/15.
 */


function PeerGraph(container, requests, width, height) {
  this.container = container;
  this.requests = requests;
  this.width = width || $(this.container).width() || 400;
  //noinspection JSSuspiciousNameCombination
  this.height = height || $(this.container).height() || 400;
  this.factor = 1;
  this.rx = this.factor * this.width / 2;
  this.ry = this.factor * this.height / 2;
  this.x = this.rx;
  this.y = this.ry;
  this.m0 = 0;
  this.rotate = 0;

  this.nodes = [];
  this.links = [];
  this.splines = [];
  this.info = {};
  this.clusters = null;
  this.clicked_node = null;
  this.map = {};
  this.links_keys = {};

  this.cluster = d3.layout.cluster();
  this.cluster
    .size([360, this.ry - 80])
    .sort(function (a, b) {
      return d3.ascending(a.key, b.key);
    });

  this.bundle = d3.layout.bundle();

  // first direction
  this.line_first = d3.svg.line.radial()
    .interpolate("bundle")
    .tension(.5)
    .radius(function (d) {
      return d.y;
    })
    .angle(function (d) {
      return d.x / 180 * Math.PI;
    });

  // second direction
  this.line_second = d3.svg.line.radial()
    .interpolate("bundle")
    .tension(.8)
    .radius(function (d) {
      return d.y;
    })
    .angle(function (d) {
      return d.x / 180 * Math.PI;
    });

  // Chrome 15 bug: <http://code.google.com/p/chromium/issues/detail?id=98951>
  var div = d3.select(this.container).insert("div")
    .attr("id", "graph_wrapper")
    .style("-webkit-backface-visibility", "hidden");

  this.svg = div.append("svg:svg")
    .attr("width", this.width)
    .attr("height", this.height)
    .append("svg:g")
    .attr("transform", "translate(" + this.x + "," + this.y + ")");

  this.svg.append("svg:path")
    .attr("class", "arc")
    .attr("d", d3.svg.arc().outerRadius(this.ry - 120).innerRadius(0).startAngle(0).endAngle(2 * Math.PI));

  // callback
  this.cb_click_provider = null;
}

PeerGraph.prototype.drawGraph = function(){
  var path = this.svg.selectAll("path.link")
    .data(this.links)
    .enter().append("svg:path")
    .attr("class", function (d) {
      return "link source-" + d.source.key + " target-" + d.target.key;
    })
    .attr("d", function (d, i) {
      if (d.first)
        return this.line_first(this.splines[i]);
      else
        return this.line_second(this.splines[i]);
    }.bind(this));

  var node = this.svg.selectAll("g.node")
    .data(this.nodes.filter(function (n) {
      return !n.children;
    }))
    .enter().append("svg:g")
    .attr("class", "node")
    .attr("id", function (d) {
      return "node-" + d.key;
    })
    .attr("transform", function (d) {
      return "rotate(" + (d.x - 90) + ")translate(" + d.y + ")";
    });

  var text = node
    .append("svg:text")
    .attr("dx", function (d) {
      return d.x < 180 ? 8 : -8;
    })
    .attr("dy", ".31em")
    .attr("text-anchor", function (d) {
      return d.x < 180 ? "start" : "end";
    })
    .attr("transform", function (d) {
      return d.x < 180 ? null : "rotate(180)";
    })
    .text(function (d) {
      return d.name;
    })
    .on("mouseover", this.mouseOver.bind(this))
    .on("mouseout", this.mouseOut.bind(this))
    .on("click", this.mouseClick.bind(this));

  // legend
  var legend_entries = [
    {name: "subscribes", y: 34},
    {name: "publishes", y: 54}
  ];
  var legend = this.svg.selectAll("g.legend")
    .data(legend_entries)
    .enter()
    .append("g")
    .attr("class", "legend")
    .attr("x", 20)
    .attr("y", function (d) {
      return 20 + d.y;
    })
    .attr("transform", "translate(-" + this.rx + ",-" + this.ry + ")")

  legend
    .append('line')
    .attr("class", function (d) {
      return d.name;
    })
    .attr("x1", 20)
    .attr("y1", function (d) {
      return d.y - 3;
    })
    .attr("x2", 60)
    .attr("y2", function (d) {
      return d.y - 3;
    });

  legend
    .append('text')
    .attr("class", function (d) {
      return d.name;
    })
    .attr('x', 70)
    .attr('y', function (d) {
      return d.y;
    })
    .text(function (d) {
      return d.name;
    });
};

PeerGraph.prototype.mouse = function(e) {
  return [e.pageX - rx, e.pageY - this.ry];
};

PeerGraph.prototype.mouseDown = function() {
  this.m0 = this.mouse(d3.event);
  d3.event.preventDefault();
};

PeerGraph.prototype.mouseMove = function() {
  if (this.m0) {
    var m1 = this.mouse(d3.event),
      dm = Math.atan2(this.cross(this.m0, m1), this.dot(this.m0, m1)) * 180 / Math.PI;
    div.style("-webkit-transform", "translateY(" + (this.ry - this.rx) + "px)rotateZ(" + dm + "deg)translateY(" + (this.rx - this.ry) + "px)");
  }
};

PeerGraph.prototype.mouseUp = function() {
  if (this.m0) {
    var m1 = this.mouse(d3.event);
    this.rotate += Math.atan2(this.cross(m0, m1), this.dot(m0, m1)) * 180 / Math.PI;
    if (this.rotate > 360) this.rotate -= 360;
    else if (this.rotate < 0) this.rotate += 360;
    this.m0 = null;

    div.style("-webkit-transform", null);

    this.svg
      .attr("transform", "translate(" + this.rx + "," + this.ry + ")rotate(" + this.rotate + ")")
      .selectAll("g.node text")
      .attr("dx", function (d) {
        return (d.x + this.rotate) % 360 < 180 ? 8 : -8;
      }.bind(this))
      .attr("text-anchor", function (d) {
        return (d.x + this.rotate) % 360 < 180 ? "start" : "end";
      }.bind(this))
      .attr("transform", function (d) {
        return (d.x + this.rotate) % 360 < 180 ? null : "rotate(180)";
      }.bind(this));
  }
};

PeerGraph.prototype.mouseOver = function(d) {
  if (this.clicked_node) return;
  this.svg.select("#node-" + d.key).classed("selected", true);

  this.svg.selectAll("path.link.target-" + d.key)
    .classed("target", true)
    .each(this.updateNodes("source", true));

  this.svg.selectAll("path.link.source-" + d.key)
    .classed("source", true)
    .each(this.updateNodes("target", true));
};

PeerGraph.prototype.mouseOut = function(d) {
  if (this.clicked_node) return;
  this.svg.select("#node-" + d.key).classed("selected", false);

  this.svg.selectAll("path.link.source-" + d.key)
    .classed("source", false)
    .each(this.updateNodes("target", false));

  this.svg.selectAll("path.link.target-" + d.key)
    .classed("target", false)
    .each(this.updateNodes("source", false));
};

PeerGraph.prototype.mouseClick = function(d) {
  if (this.cb_click_provider) {
    this.cb_click_provider(d);
  }
  if (this.clicked_node) {
    var tmp = this.clicked_node;
    this.clicked_node = null;
    this.mouseOut(tmp);
    if (tmp == d) return;
  }
  this.mouseOver(d);
  this.clicked_node = d;

};


PeerGraph.prototype.updateNodes = function(name, value) {
  var g = this;
  return function (d) {
    // if d in its own peers ... return
    if (value) this.parentNode.appendChild(this);
    g.svg.select("#node-" + d[name].key).classed(name, value);
  };
};

PeerGraph.prototype.cross = function(a, b) {
  return a[0] * b[1] - a[1] * b[0];
};

PeerGraph.prototype.dot = function(a, b) {
  return a[0] * b[0] + a[1] * b[1];
};

PeerGraph.prototype.add_link = function(link) {
  this.links.append(link);
  this.drawGraph(this.nodes, this.links, this.bundle(this.links));
};

PeerGraph.prototype.find_cluster = function(name, data) {
  var node = this.map[name];
  if (!node) {
    node = this.map[name] = data || {name: name, children: [], subnet: ""};
    if (name.length) {
      node.parent = this.find_cluster(node.subnet);
      node.parent.children.push(node);
      node.key = name.replace(/\./g, "_");
    }
  }
  return node;
};

PeerGraph.prototype.create_clusters = function(classes) {

  var g = this;
  classes.forEach(function (d) {
    this.find_cluster(d.name, d);
  }.bind(this));

  return this.map[""];
};

PeerGraph.prototype.peers = function(peer_nodes) {
  var peers = [];

  // For each peer, construct a link from the source to target node.
  peer_nodes.forEach(function (d) {
    if (!this.map[d.name]) {
      this.map[d.name] = d;
    }
    if (d.peers) d.peers.forEach(
      function (i) {
        peers.push({source: this.map[d.name], target: this.map[i]});
      }.bind(this)
    );
  }.bind(this));

  return peers;
};

PeerGraph.prototype.start = function(config) {
  //config = {ip_address: "10.1.1.12", port: "20001"};
  //this.requests.add_dispatcher(config);
  this.requests.dispatcher("provider_agent_clusters", {
    success: function (data) {
      var networks = data.providers;
      this.clusters = this.create_clusters(networks);
      this.nodes = this.cluster.nodes(this.clusters);
      this.links = this.peers(this.nodes);
      this.splines = this.bundle(this.links);
      this.drawGraph();
      for (var i = 0; i < this.nodes.length; i++) {
        var node = this.nodes[i];
        if (node.subnet.length) {
          this.requests.provider(node.name, node.port, "agent_peers_info", {
            success: function (data) {
              if (!data) {
                return;
              }
              if (!data.result) {
                console.error("Error", data["msg"], data);
                return;
              }
              var source = this.map[data.ip_address];
              if (!source) {
                return;
              }
              // add edges for peers
              for (var i = 0; i < data.peers.length; i++) {
                var target = this.map[data.peers[i]];
                if (!target) {
                  return;
                }
                var link = {source: source, target: target, first: true};
                if (this.links_keys[target.name + "_" + source.name]) {
                  link.first = false;
                }
                this.links.push(link);
                this.links_keys[source.name + "_" + target.name] = true;

                this.splines = this.bundle(this.links);
                this.drawGraph();
              }
            }.bind(this)
          })
        }
      }
    }.bind(this)
  });
};

angular.module('lamaTowerApp')
  .directive('peerGraph', function (lamaConfig, lamaRequests) {
    return {
      restrict: 'E',
      link: function (scope, element, attrs) {
        //element.css({"min-width" : "400px"});
        //element.width(400);
        //element.height(400);
        var pg = new PeerGraph(element[0], lamaRequests);
        if (scope.provider_clicked)
          pg.cb_click_provider = scope.provider_clicked;
        pg.start();
        //{
        //  "ip_address": lamaConfig.dispatcher.ip,
        //  "port": lamaConfig.dispatcher.port
        //});

      }}
  });

