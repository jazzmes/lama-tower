/**
 * Created by tiago on 2/18/16.
 */

angular.module('lamaTowerApp')
  .directive('realTimeArea', function ($window) {
    return {
      link: function (scope, element, attrs) {
        var d = scope[attrs.chartKey];
        if (!d) {
          console.error("Define real time area chart key");
          return;
        }
        var opts = d.options || {};
        opts.type = 'time.area';
        if (!opts.height) {
          opts.height = element.height() || element.parent().height();
        }
        if (!opts.width) {
          opts.width = element.width() || element.parent().width();
        }
        opts.data = d.data;
        var live_chart = element.epoch(opts);

        scope.$watch(
          function () {
            return {
              width: element.parent().width(),
              height: element.parent().height(),
            }
          },
          function () {
            live_chart.option('width', element.parent().width());
          }, //listener
          true //deep watch
        );

        scope.$watch(
          'response_time_chart.feed',
          function() {
            if (scope.response_time_chart.feed)
              live_chart.push(scope.response_time_chart.feed);
          },
          true
        )
      },
      replace: true,
      template: '<div class="epoch"></div>'
    }
  });
