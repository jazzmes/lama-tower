/**
 * Created by tiago on 2/18/16.
 */
angular.module('lamaTowerApp')
  .directive('dynTable', function () {
    return function (scope, element, attrs) {
      console.log("@dyntable", attrs, scope);
      var ref = scope[attrs.dynTable];
      //console.log(attrs.dynTable,/ scope, scope.providerInfo);
      if (!ref){
        console.error("directive dynTable ERROR: no reference found in dynTable attribute");
        return;
      }
      this.datatable = null;
      //function to redraw table in push mode...
      this.drawTable = function (newValue, oldValue) {
        console.log(newValue, oldValue);
        if (newValue && newValue.headers
          && (!$.fn.DataTable.fnIsDataTable(element) || JSON.stringify(newValue) != JSON.stringify(oldValue))) {
          var options = {
            "data": newValue.data,
            "columns": newValue.headers,
            "bDestroy": true
          };
          $.extend(options, newValue.options);

          console.log("1. start dirty stuff..");
          if (this.datatable) {
            // the inneficient part...
            this.datatable.clear();
            if (newValue.data) {
              console.log("2. should not be here..");
              $.each(newValue.data, function(i, v){
                //console.log("adding", v, this.datatable);
                this.datatable.row.add(v);
              }.bind(this));
            }
            this.datatable.draw();
          }
  //        else {
  //          console.log("2. should be here..", options);
  //          this.datatable = element.DataTable(options);
  ////                        this.datatable
  ////                            .on('click', 'tr', function (data, x, y, z) {
  ////                                console.log( data, x, y, z );
  ////                            });
  //        }
          console.log("3. end");
        }
      };
      if (ref.autoreload){
        scope.$watch(attrs.dynTable, drawTable, true);
      } else {
        this.drawTable(ref, null);
      }
    };
  });
